package pl.houseofjava.quotes.notification.service;

import android.app.PendingIntent;
import android.content.Context;

import pl.houseofjava.quotes.notification.dao.preferences.CommonNotificationDao;

public abstract class NotificationService {
    protected boolean enforceAlarmForTomorrow;

    public abstract void run(PendingIntent pendingIntent);

    public static class Builder {
        boolean enforceAlarmForTomorrow;

        public Builder withEnforcingNextDayAlarm() {
            this.enforceAlarmForTomorrow = true;
            return this;
        }

        public NotificationService build(Context context) {
            CommonNotificationDao commonNotificationSettings = new CommonNotificationDao(context);

            NotificationService notificationService = null;
            switch (commonNotificationSettings.getStrategy()) {
                case Random:
                    notificationService = new RandomNotificationService(context);
                    notificationService.enforceAlarmForTomorrow = this.enforceAlarmForTomorrow;
                    break;
                case Constant:
                    notificationService = new ConstantNotificationService(context);
                    notificationService.enforceAlarmForTomorrow = this.enforceAlarmForTomorrow;
                    break;
                default:
                    notificationService = new RandomNotificationService(context);
                    break;
            }
            return notificationService;
        }
    }
}
