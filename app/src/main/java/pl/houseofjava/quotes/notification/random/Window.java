package pl.houseofjava.quotes.notification.random;

import org.joda.time.DateTime;

public class Window {
    private DateTime start;
    private DateTime end;

    private Window(DateTime start, DateTime end) {
        this.start = start;
        this.end = end;
    }

    public DateTime getStart() {
        return start;
    }

    public DateTime getEnd() {
        return end;
    }

    public static class Builder {
        private DateTime start;
        private DateTime end;
        private boolean duringMidnight = false;


        public Builder withStartHourMinute(DateTime start) {
            this.start = start;
            return this;
        }

        public Builder withEndHourMinute(DateTime end) {
            this.end = end;
            return this;
        }

        public Builder setDuringMidnight(boolean duringMidnight) {
            this.duringMidnight = duringMidnight;
            return this;
        }

        public Window build(DateTime now) {
            DateTime start;
            DateTime end;
            if (duringMidnight) {
                start = now.withMillisOfDay(this.start.getMillisOfDay());
                end = now.plusDays(1).withTimeAtStartOfDay().plusMillis(this.end.getMillisOfDay());
            } else {
                start = now.withMillisOfDay(this.start.getMillisOfDay());
                end = now.withMillisOfDay(this.end.getMillisOfDay());
            }
            return new Window(start, end);
        }
    }
}
