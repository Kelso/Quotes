package pl.houseofjava.quotes.notification.dao.sql;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import pl.houseofjava.quotes.notification.model.Quote;

import static pl.houseofjava.quotes.notification.dao.sql.QuoteSQLiteHelper.*;

public class QuoteDao {
    private SQLiteDatabase db;
    private QuoteSQLiteHelper dbHelper;

    public QuoteDao(Context context, String ISO3Language) {
        dbHelper = new QuoteSQLiteHelper(context, ISO3Language);
        db = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public List<Integer> getIdsWithSmallestCount() {
        Cursor cursor = db.rawQuery("SELECT _id FROM quote q WHERE q.counter IN (SELECT MIN(q2.counter) FROM quote q2);", new String[0]);

        return getResultList(cursor, Integer.class);
    }

    public void updateCounter(int id, int counter, int lastDisplayTimestamp) {
        db.execSQL("UPDATE quote SET counter = ?, last_display_timestamp = ? WHERE _id = ? ;",
                new String[]{String.valueOf(counter), String.valueOf(lastDisplayTimestamp), String.valueOf(id)});
    }

    public List<Quote> findQuotes(Integer id, Integer counter, String origin) {
        StringBuilder query = new StringBuilder("SELECT q._id, q.counter, qb.body, q.origin, q.last_display_timestamp FROM quote q JOIN " + dbHelper.getQuoteBodyTableName() + " qb ON q._id=qb.quote_id ");
        List<String> selectionArgs = new ArrayList<>();
        prepareWhereClause(query, selectionArgs, id, counter, origin);
        query.append(";");

        Cursor cursor = db.rawQuery(query.toString(), selectionArgs.toArray(new String[selectionArgs.size()]));
        return getResultList(cursor, Quote.class);
    }

    public void insertQuote(Quote quote) {
        db.execSQL("INSERT INTO quote (counter, origin) VALUES(?,?);", new String[]{String.valueOf(quote.getCounter()), quote.getOrigin()});
        db.execSQL("INSERT INTO quote_body_pl (body, quote_id) VALUES(?, (SELECT _id FROM quote q WHERE q.origin=?));", new String[]{quote.getBody(), quote.getOrigin()});
    }

    public void clearQuotes() {
        db.execSQL("DELETE FROM quote;");
    }
}

