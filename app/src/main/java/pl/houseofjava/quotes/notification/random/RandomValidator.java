package pl.houseofjava.quotes.notification.random;


import pl.houseofjava.quotes.R;
import pl.houseofjava.quotes.notification.dao.preferences.RandomNotificationDao;
import pl.houseofjava.quotes.notification.util.NotificationException;
import pl.houseofjava.quotes.notification.util.Validate;

import static pl.houseofjava.quotes.notification.random.RandomNotificationUtil.canNotify;

public class RandomValidator {

    public void windowIsLongEnough(RandomNotificationDao randomSettings) throws NotificationException {
        Validate.isTrue(canNotify(randomSettings.getDayStartHourMinute(),
                randomSettings.getDayEndHourMinute(),
                Integer.valueOf(randomSettings.getCounter())),
                R.string.ui_toast_random_validator);
    }
}
