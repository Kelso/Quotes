package pl.houseofjava.quotes.notification.util;


import junit.framework.Assert;

import pl.houseofjava.quotes.notification.model.Quote;

public class QuoteAssert {

    private Quote actual;

    private QuoteAssert(Quote actual) {
        this.actual = actual;
    }

    public static QuoteAssert assertThat(Quote quote) {
        return new QuoteAssert(quote);
    }

    public QuoteAssert hasBody(String body) {
        Assert.assertEquals(body, actual.getBody());
        return this;
    }

    public QuoteAssert hasOrigin(String origin) {
        Assert.assertEquals(origin, actual.getOrigin());
        return this;
    }

    public QuoteAssert hasCounter(int count) {
        Assert.assertEquals(count, actual.getCounter());
        return this;
    }

    public QuoteAssert hasId(int id) {
        Assert.assertEquals(id, actual.getId());
        return this;
    }
}
