package pl.houseofjava.quotes.notification.dao.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import org.joda.time.DateTime;

import pl.houseofjava.quotes.R;
import pl.houseofjava.quotes.notification.model.RandomNotificationData;
import pl.houseofjava.quotes.notification.model.RandomNotificationDataBuilder;
import pl.houseofjava.quotes.notification.random.constants.Counter;
import pl.houseofjava.quotes.notification.random.constants.MinutesToEndDay;

import static java.lang.String.valueOf;
import static pl.houseofjava.quotes.notification.util.DateUtil.getDateTime;
import static pl.houseofjava.quotes.notification.util.DateUtil.getMinutes;

public class RandomNotificationDao extends NotificationDao {

    public RandomNotificationDao(Context context) {
        super(context);
    }

    @Override
    public void storeIntoPreferences() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(context.getString(R.string.random_counter), getCounter());
        editor.putString(context.getString(R.string.random_current_counter), valueOf(getCounter()));
        editor.putInt(context.getString(R.string.random_period_start_hour_minute), getDayStartHourMinute().getMinuteOfDay());
        editor.putInt(context.getString(R.string.random_period_end_hour_minute), getDayEndHourMinute().getMinuteOfDay());
        editor.putInt(MinutesToEndDay.KEY, getMinutesToEndOfDay());
        editor.apply();
    }

    public String getCounter() {
        return sharedPreferences.getString(context.getString(R.string.random_counter), valueOf(Counter.Default.getValue()));
    }

    public String getCurrentCounter() {
        return sharedPreferences.getString(context.getString(R.string.random_current_counter), getCounter());
    }

    public void setCurrentCounter(int counter) {
        sharedPreferences.edit().putString(context.getString(R.string.random_current_counter), valueOf(counter)).apply();
    }

    public DateTime getDayStartHourMinute() {
        return getDateTime(sharedPreferences.getInt(context.getString(R.string.random_period_start_hour_minute), getMinutes(9, 0)));
    }

    public DateTime getDayEndHourMinute() {
        return getDateTime(sharedPreferences.getInt(context.getString(R.string.random_period_end_hour_minute), getMinutes(21, 0)));
    }

    public int getMinutesToEndOfDay() {
        return sharedPreferences.getInt(MinutesToEndDay.KEY, MinutesToEndDay.Default.getValue());
    }

    public RandomNotificationData getRandomNotificationData() {
        return new RandomNotificationDataBuilder()
                .withCounter(Integer.valueOf(getCounter()))
                .withCurrentCounter(Integer.valueOf(getCurrentCounter()))
                .withDayEndHourMinute(getDayEndHourMinute())
                .withDayStartHourMinute(getDayStartHourMinute())
                .build();
    }

}
