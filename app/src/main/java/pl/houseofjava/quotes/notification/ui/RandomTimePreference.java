package pl.houseofjava.quotes.notification.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TimePicker;

import org.joda.time.DateTime;

import pl.houseofjava.quotes.R;
import pl.houseofjava.quotes.notification.util.DateUtil;

import static android.text.format.DateFormat.is24HourFormat;
import static pl.houseofjava.quotes.notification.util.DateUtil.UNSET_NOTIFICATION;
import static pl.houseofjava.quotes.notification.util.DateUtil.toHourMinute;

public class RandomTimePreference extends DialogPreference {

    private DateTime calendar = null;
    private TimePicker picker = null;


    public RandomTimePreference(Context ctx) {
        this(ctx, null);
    }

    public RandomTimePreference(Context ctxt, AttributeSet attrs) {
        this(ctxt, attrs, android.R.attr.dialogPreferenceStyle);
    }

    public RandomTimePreference(Context ctxt, AttributeSet attrs, int defStyle) {
        super(ctxt, attrs, defStyle);

        setPositiveButtonText(R.string.ui_settings_date_picker_set);
        setNegativeButtonText(R.string.ui_settings_date_picker_cancel);
    }

    @Override
    protected View onCreateDialogView() {
        picker = new TimePicker(getContext());
        picker.setIs24HourView(is24HourFormat(getContext()));
        return picker;
    }

    @Override
    protected void onBindDialogView(View v) {
        super.onBindDialogView(v);
        if (calendar == null) {
            calendar = new DateTime();
        }
        picker.setCurrentHour(calendar.getHourOfDay());
        picker.setCurrentMinute(calendar.getMinuteOfHour());
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);

        if (positiveResult) {
            calendar = calendar.withHourOfDay(picker.getCurrentHour()).withMinuteOfHour(picker.getCurrentMinute());
            setSummary(getSummary());
            if (callChangeListener(calendar.getMinuteOfDay())) {
                persistInt(calendar.getMinuteOfDay());
                notifyChanged();
            }
        }
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return (a.getString(index));
    }

    @Override
    protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {
        if (restoreValue) {
            calendar = DateUtil.getDateTime(getPersistedInt(UNSET_NOTIFICATION));
        } else {
            if (defaultValue == null) {
                calendar = DateTime.now();
            } else {
                calendar = DateUtil.getDateTime(getPersistedInt(UNSET_NOTIFICATION));
            }
        }
        setSummary(getSummary());
    }

    @Override
    public CharSequence getSummary() {
        if (calendar == null) {
            return null;
        }
        return toHourMinute(calendar);
    }
}
