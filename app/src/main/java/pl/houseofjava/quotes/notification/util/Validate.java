package pl.houseofjava.quotes.notification.util;


public class Validate {
    public static void isTrue(boolean condition, int stringId) throws NotificationException {
        if (!condition) {
            throw new NotificationException(stringId);
        }
    }
}
