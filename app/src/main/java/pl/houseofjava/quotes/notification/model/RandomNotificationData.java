package pl.houseofjava.quotes.notification.model;


import org.joda.time.DateTime;

import static pl.houseofjava.quotes.notification.random.constants.Counter.NO_NOTIFICATIONS_FOR_TODAY_COUNTER_VALUE;

public class RandomNotificationData {
    private DateTime dayStartHourMinute;
    private DateTime dayEndHourMinute;

    private DateTime lastNotificationTime;

    private int counter;
    private int currentCounter = NO_NOTIFICATIONS_FOR_TODAY_COUNTER_VALUE;

    private int minutesPeriod;
    private DateTime periodStart;

    public RandomNotificationData() {
    }

    public DateTime getDayStartHourMinute() {
        return dayStartHourMinute;
    }

    public void setDayStartHourMinute(DateTime dayStartHourMinute) {
        this.dayStartHourMinute = dayStartHourMinute;
    }

    public DateTime getPeriodStart() {
        return periodStart;
    }

    public void setPeriodStart(DateTime periodStart) {
        this.periodStart = periodStart;
    }

    public DateTime getDayEndHourMinute() {
        return dayEndHourMinute;
    }

    public void setDayEndHourMinute(DateTime dayEndHourMinute) {
        this.dayEndHourMinute = dayEndHourMinute;
    }

    public Integer getCounter() {
        return counter;
    }

    public void setCounter(Integer counter) {
        this.counter = counter;
    }

    public int getCurrentCounter() {
        return currentCounter;
    }

    public void setCurrentCounter(int currentCounter) {
        this.currentCounter = currentCounter;
    }

    public Integer getMinutesPeriod() {
        return minutesPeriod;
    }

    public void setMinutesPeriod(int minutesPeriod) {
        this.minutesPeriod = minutesPeriod;
    }

    public DateTime getLastNotificationTime() {
        return lastNotificationTime;
    }

    public void setLastNotificationTime(DateTime lastNotificationTime) {
        this.lastNotificationTime = lastNotificationTime;
    }
}
