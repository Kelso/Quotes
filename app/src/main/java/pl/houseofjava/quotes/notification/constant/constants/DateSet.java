package pl.houseofjava.quotes.notification.constant.constants;

import org.joda.time.DateTime;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.util.Arrays.asList;
import static pl.houseofjava.quotes.notification.util.DateUtil.getDateTime;


public enum DateSet {
    Default(getDateTime(9, 30), getDateTime(19, 30));

    public final static List<String> KEYS = asList("alarmHourMinute1", "alarmHourMinute2", "alarmHourMinute3", "alarmHourMinute4");

    private Set<DateTime> alarms = new HashSet<>();

    DateSet(DateTime... pairs) {
        alarms.addAll(asList(pairs));
    }

    public Set<DateTime> getAlarms() {
        return alarms;
    }

}
