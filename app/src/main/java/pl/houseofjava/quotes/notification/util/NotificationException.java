package pl.houseofjava.quotes.notification.util;


public class NotificationException extends Exception {
    private int stringId;

    public NotificationException(int stringId) {
        super();
        this.stringId = stringId;
    }

    public int getStringId() {
        return stringId;
    }

}
