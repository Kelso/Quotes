package pl.houseofjava.quotes.notification.model;

import org.joda.time.DateTime;

import java.util.List;


public class ConstantNotificationDataBuilder {
    private List<DateTime> notificationTimes;
    private boolean enforceAlarmForNextDay;
    private DateTime newNotificationTime;

    public ConstantNotificationDataBuilder withNotificationTimes(List<DateTime> notificationTimes) {
        this.notificationTimes = notificationTimes;
        return this;
    }

    public ConstantNotificationDataBuilder withEnforceAlarmForNextDay(boolean enforceAlarmForNextDay) {
        this.enforceAlarmForNextDay = enforceAlarmForNextDay;
        return this;
    }

    public ConstantNotificationDataBuilder withNewNotificationTime(DateTime newNotificationTime) {
        this.newNotificationTime = newNotificationTime;
        return this;
    }

    public ConstantNotificationData build() {
        ConstantNotificationData constantNotificationData = new ConstantNotificationData(enforceAlarmForNextDay, notificationTimes);
        constantNotificationData.setNewNotificationTime(newNotificationTime);

        return constantNotificationData;
    }


}
