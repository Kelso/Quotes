package pl.houseofjava.quotes.notification.util.logging;

/**
 * Created by dmn on 27.07.16.
 */
public interface Loggable {

    String getLogTag();
}
