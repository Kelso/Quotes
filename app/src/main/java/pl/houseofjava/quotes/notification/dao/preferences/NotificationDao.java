package pl.houseofjava.quotes.notification.dao.preferences;


import android.content.Context;
import android.content.SharedPreferences;

import pl.houseofjava.quotes.notification.component.activity.MainSettingsActivity;

public abstract class NotificationDao {

    protected Context context;
    protected SharedPreferences sharedPreferences;


    public NotificationDao(Context context) {
        this.context = context.getApplicationContext();
        this.sharedPreferences = context.getSharedPreferences(MainSettingsActivity.PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    public abstract void storeIntoPreferences();

    public Context getContext() {
        return context;
    }
}
