package pl.houseofjava.quotes.notification.dao.preferences;


import android.content.Context;
import android.content.SharedPreferences;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

import pl.houseofjava.quotes.notification.model.ConstantNotificationData;
import pl.houseofjava.quotes.notification.model.ConstantNotificationDataBuilder;
import pl.houseofjava.quotes.notification.util.DateUtil;

import static pl.houseofjava.quotes.notification.constant.constants.DateSet.Default;
import static pl.houseofjava.quotes.notification.constant.constants.DateSet.KEYS;
import static pl.houseofjava.quotes.notification.util.DateUtil.UNSET_NOTIFICATION;

public class ConstantNotificationDao extends NotificationDao {

    public ConstantNotificationDao(Context context) {
        super(context);
    }

    @Override
    public void storeIntoPreferences() {
        if (anyNotificationEnabled()) {
            return;
        }

        SharedPreferences.Editor editor = sharedPreferences.edit();
        int i = 0;
        for (DateTime dateTime : Default.getAlarms()) {
            editor.putInt(KEYS.get(i++), dateTime.getMinuteOfDay());
        }
        editor.apply();
    }

    private Set<DateTime> getDatesSet() {
        Set<DateTime> datesSet = new TreeSet<>();
        for (String key : KEYS) {
            int minutesOfDay = findMinutesOfDay(key);
            if (minutesOfDay != UNSET_NOTIFICATION) {
                datesSet.add(DateUtil.getDateTime(minutesOfDay));
            }
        }
        return datesSet;
    }

    public boolean anyNotificationEnabled() {
        return !getDatesSet().isEmpty();
    }

    private int findMinutesOfDay(String key) {
        return sharedPreferences.getInt(key, UNSET_NOTIFICATION);
    }

    public ConstantNotificationData getNotificationData(boolean enforceNextDay) {
        return new ConstantNotificationDataBuilder().withNotificationTimes(new ArrayList<>(getDatesSet())).withEnforceAlarmForNextDay(enforceNextDay).build();
    }
}
