package pl.houseofjava.quotes.notification.util.logging;


import android.util.Log;

public class Logger {
    private final static String logTag = "hoj.";

    public static void logDebug(String tag, String message, Object... values) {
        Log.d(logTag + tag, String.format(message, values));
    }

    public static void logInfo(String tag, String message, Object... values) {
        Log.i(logTag + tag, String.format(message, values));
    }

    public static void logWarn(String tag, String message, Object... values) {
        Log.w(logTag + tag, String.format(message, values));
    }
}
