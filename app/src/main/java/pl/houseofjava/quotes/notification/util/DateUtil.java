package pl.houseofjava.quotes.notification.util;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Locale;

public class DateUtil {
    public static final int UNSET_NOTIFICATION = -1;
    public static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.shortTime().withLocale(Locale.getDefault());

    public static boolean isAfterRegardingTime(DateTime time, DateTime referenceTime) {
        return time.getMillisOfDay() > referenceTime.getMillisOfDay();
    }

    public static DateTime getDateTime(Integer minutesOfDay) {
        if (minutesOfDay == null || minutesOfDay.equals(UNSET_NOTIFICATION)) {
            return null;
        }
        return new DateTime().withTimeAtStartOfDay().plusMinutes(minutesOfDay);
    }

    public static DateTime getDateTime(int hours, int minutes) {
        return new DateTime().withTimeAtStartOfDay().withHourOfDay(hours).withMinuteOfHour(minutes);
    }

    public static String toHourMinute(DateTime dateTime) {
        return dateTimeFormatter.print(dateTime);
    }

    public static int getMinutes(int hours, int minutes) {
        return hours * 60 + minutes;
    }
}
