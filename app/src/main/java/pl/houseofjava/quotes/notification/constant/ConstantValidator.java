package pl.houseofjava.quotes.notification.constant;

import pl.houseofjava.quotes.R;
import pl.houseofjava.quotes.notification.dao.preferences.ConstantNotificationDao;
import pl.houseofjava.quotes.notification.util.NotificationException;
import pl.houseofjava.quotes.notification.util.Validate;

public class ConstantValidator {

    public void anyAlarmEnabled(ConstantNotificationDao constantSettings) throws NotificationException {
        Validate.isTrue(constantSettings.anyNotificationEnabled(), R.string.ui_constant_validator);
    }

}
