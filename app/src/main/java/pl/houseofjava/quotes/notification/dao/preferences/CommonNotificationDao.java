package pl.houseofjava.quotes.notification.dao.preferences;

import android.content.Context;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import pl.houseofjava.quotes.R;
import pl.houseofjava.quotes.notification.common.constants.Strategy;

public class CommonNotificationDao extends NotificationDao {

    public CommonNotificationDao(Context context) {
        super(context);
    }

    public boolean isNotificationRunning() {
        return sharedPreferences.getBoolean(context.getString(R.string.alarmEnable), false);
    }

    public boolean isNoStrategyLoaded() {
        return sharedPreferences.getString(context.getString(R.string.strategyType), null) == null;
    }

    public void storeNotificationRunningState(boolean notificationRunning) {
        sharedPreferences.edit()
                .putBoolean(context.getString(R.string.alarmEnable), notificationRunning)
                .apply();
    }

    public void storeLanguage(String language) {
        sharedPreferences.edit()
                .putString(context.getString(R.string.language), language)
                .apply();

    }

    public Strategy getStrategy() {
        return Strategy.valueOfString(sharedPreferences.getString(context.getString(R.string.strategyType), Strategy.Random.getValue()));
    }

    public String getLanguage() {
        return sharedPreferences.getString(context.getString(R.string.language), Locale.getDefault().getISO3Language());
    }

    @Override
    public void storeIntoPreferences() {
        storeNotificationRunningState(false);

        List<String> iso3Languages = Arrays.asList(getContext().getResources().getStringArray(R.array.notification_language_values_array));
        String iso3Language = Locale.getDefault().getISO3Language();
        if (!iso3Languages.contains(iso3Language)) {
            iso3Language = iso3Languages.get(0);
        }
        storeLanguage(iso3Language);
    }
}
