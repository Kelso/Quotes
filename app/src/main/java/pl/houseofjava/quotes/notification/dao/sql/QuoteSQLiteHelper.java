package pl.houseofjava.quotes.notification.dao.sql;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import pl.houseofjava.quotes.R;
import pl.houseofjava.quotes.notification.model.Quote;
import pl.houseofjava.quotes.notification.util.NumberUtil;
import pl.houseofjava.quotes.notification.util.logging.Loggable;
import pl.houseofjava.quotes.notification.util.logging.Logger;

public class QuoteSQLiteHelper extends SQLiteOpenHelper implements Loggable {
    // iso639-2


    public static final String DATABASE_NAME = "quote";

    public static final String TABLE_NAME = "quote";
    public static final String TABLE_NAME_PL = "quote_body_pol";
    public static final String TABLE_NAME_EN = "quote_body_eng";
    public static final String TABLE_NAME_ES = "quote_body_esp";

    public static final String COLUMN_COUNTER = "counter";
    public static final String COLUMN_BODY = "body";
    public static final String COLUMN_ORIGIN = "origin";
    public static final String COLUMN_LAST_DISPLAY_TIMESTAMP = "last_display_timestamp";

    private static final int DATABASE_VERSION = 1;
    private static final Integer NUMBER_OF_QUOTES = 2;

    private final Context context;
    private final String ISO3language;

    private static String QUOTE_DDL = "CREATE TABLE " + TABLE_NAME + " ( " +
            "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_COUNTER + " INTEGER NOT NULL DEFAULT 0, " +
            COLUMN_ORIGIN + " TEXT NOT NULL, " +
            COLUMN_LAST_DISPLAY_TIMESTAMP + " INTEGER);";

    private static String QUOTE_BODY_PL_DDL = "CREATE TABLE " + TABLE_NAME_PL +
            "(_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_BODY + " TEXT NOT NULL, " +
            "quote_id INTEGER, " +
            "FOREIGN KEY (quote_id) REFERENCES quote(_id));";

    private static String QUOTE_BODY_EN_DDL = "CREATE TABLE " + TABLE_NAME_EN +
            "(_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_BODY + " TEXT NOT NULL, " +
            "quote_id INTEGER," +
            "FOREIGN KEY (quote_id) REFERENCES quote(_id));";

    private static String QUOTE_BODY_ES_DDL = "CREATE TABLE " + TABLE_NAME_ES +
            "(_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_BODY + " TEXT NOT NULL, " +
            "quote_id INTEGER," +
            "FOREIGN KEY (quote_id) REFERENCES quote(_id));";

    public QuoteSQLiteHelper(Context context, String ISO3Language) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        this.ISO3language = ISO3Language;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(QUOTE_DDL);
        db.execSQL(QUOTE_BODY_PL_DDL);
        db.execSQL(QUOTE_BODY_EN_DDL);
        db.execSQL(QUOTE_BODY_ES_DDL);

        if (applyPatches(db)) {
            logPatchesApplied(db);
        } else {
            Logger.logWarn(getLogTag(), "No quote has been applied to database.");
        }
    }

    public boolean applyPatches(SQLiteDatabase database) {
        return applyPatch(database, R.raw.quotes_1);
    }

    private boolean applyPatch(SQLiteDatabase db, int resourceId) {
        InputStream is = context.getResources().openRawResource(resourceId);
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        int index = 0;
        try {
            int content;
            StringBuilder sqlSript = new StringBuilder();
            while ((content = br.read()) != -1) {
                char contentChr = (char) content;
                sqlSript.append(contentChr);
                if (contentChr == ';') {
                    db.execSQL(sqlSript.toString());
                    index++;
                    sqlSript = new StringBuilder();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return index > 0;
        }
    }

    private void logPatchesApplied(SQLiteDatabase db) {
        Cursor cursor = db.rawQuery("SELECT COUNT() FROM " + TABLE_NAME + " ;", new String[0]);
        int appliedQuotesNo = NumberUtil.toPrimitiveInt(getResult(cursor, Integer.class));

        Logger.logInfo(getLogTag(), "Applied %s/%s quotes.", appliedQuotesNo, NUMBER_OF_QUOTES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public static <T> List<T> getResultList(Cursor cursor, Class<T> clazz) {
        List<T> result = new ArrayList<>();
        if (cursor.getCount() == 0) {
            return result;
        }

        cursor.moveToFirst();
        do {
            result.add(getResult(cursor, clazz, false));
        }
        while (cursor.moveToNext());
        cursor.close();

        return result;
    }

    private <T> T getResult(Cursor cursor, Class<T> clazz) {
        if (cursor == null || cursor.getCount() == 0) {
            return null;
        }
        cursor.moveToFirst();
        return getResult(cursor, clazz, true);
    }

    private static <T> T getResult(Cursor cursor, Class<T> clazz, boolean autoClose) {
        T result = null;
        if (clazz.isAssignableFrom(Integer.class)) {
            result = clazz.cast(cursor.getInt(0));
        } else if (clazz.isAssignableFrom(Quote.class)) {
            result = clazz.cast(transform(cursor));
        }

        if (autoClose) {
            cursor.close();
        }

        return result;
    }

    private static Quote transform(Cursor cursor) {
        return new Quote.Builder()
                .withId(cursor.getInt(0))
                .withCounter(cursor.getInt(1))
                .withBody(cursor.getString(2))
                .withOrigin(cursor.getString(3))
                .withLastDisplayedTimestamp(cursor.getInt(4))
                .build();
    }

    public static void prepareWhereClause(StringBuilder query, List<String> selectionArgs, Integer id, Integer counter, String origin) {
        List<String> conditions = new ArrayList<>();

        fillConditionsAndArgs(id, counter, origin, conditions, selectionArgs);
        appendWhereClause(query, conditions);
    }

    private static void fillConditionsAndArgs(Integer id, Integer counter, String origin, List<String> conditions, List<String> selectionArgs) {
        if (id != null) {
            conditions.add("q._id=? ");
            selectionArgs.add(String.valueOf(id));
        }

        if (counter != null) {
            conditions.add("q.counter=? ");
            selectionArgs.add(String.valueOf(counter));
        }

        if (origin != null) {
            conditions.add("q.origin=? ");
            selectionArgs.add(origin);
        }
    }

    private static void appendWhereClause(StringBuilder query, List<String> conditions) {
        int i = 1;
        if (conditions.size() > 0) {
            query.append("WHERE ");
        }

        for (String condition : conditions) {
            query.append(condition);
            if (i < conditions.size()) {
                query.append("AND ");
            }
            i++;
        }
    }

    public String getQuoteBodyTableName() {
        return "quote_body_" + ISO3language;
    }

    @Override
    public String getLogTag() {
        return "QuoteSQLiteHelper";
    }
}
