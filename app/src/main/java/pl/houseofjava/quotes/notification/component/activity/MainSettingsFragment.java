package pl.houseofjava.quotes.notification.component.activity;

import android.content.Intent;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.SwitchPreference;

import pl.houseofjava.quotes.R;
import pl.houseofjava.quotes.notification.component.broadcast.NotificationBroadcastReceiver;
import pl.houseofjava.quotes.notification.dao.preferences.CommonNotificationDao;

import static pl.houseofjava.quotes.R.string.alarmEnable;
import static pl.houseofjava.quotes.R.string.artificialKey1_strategySettings;
import static pl.houseofjava.quotes.R.string.language;
import static pl.houseofjava.quotes.R.string.strategyType;

public class MainSettingsFragment extends CustomPreferenceFragment {

    private Preference.OnPreferenceChangeListener alarmEnableSwitchListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            SwitchPreference switchPreference = (SwitchPreference) preference;
            boolean preferenceChanged = true;

            if (isSwitchToEnable(switchPreference, (Boolean) newValue)) {
                CommonNotificationDao constantNotificationDao = new CommonNotificationDao(getActivity());
                if (areAlarmsValid(constantNotificationDao)) {
                    ((MainSettingsActivity) getActivity()).initializeNotifications();
                } else {
                    switchPreference.setChecked(false);
                    preferenceChanged = false;
                }
            }
            return preferenceChanged;
        }

        private boolean isSwitchToEnable(SwitchPreference switchPreference, Boolean newValue) {
            return switchPreference.isChecked() != newValue && newValue;
        }

    };

    private Preference.OnPreferenceChangeListener strategyTypesListListener = new Preference.OnPreferenceChangeListener() {
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            CharSequence newEntry = ((ListPreference) preference).getEntries()[Integer.valueOf(newValue.toString())];
            preference.setSummary(newEntry);
            return true;
        }
    };

    private Preference.OnPreferenceChangeListener languageListListener = new Preference.OnPreferenceChangeListener() {
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            CharSequence newEntry = ((ListPreference) preference).getEntries()[getIndex(newValue)];
            preference.setSummary(newEntry);
            return true;
        }

        //ToDo add test
        private int getIndex(Object newValue) {
            String[] stringArray = getActivity().getResources().getStringArray(R.array.notification_language_values_array);
            int index = 0;
            for (int i = 0; i < stringArray.length; i++) {
                if (stringArray[i].equals(newValue)) {
                    index = i;
                }
            }
            return index;
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);

        findPreference(getString(alarmEnable)).setOnPreferenceChangeListener(alarmEnableSwitchListener);

        final ListPreference strategyList = (ListPreference) findPreference(getString(strategyType));
        strategyList.setOnPreferenceChangeListener(strategyTypesListListener);

        findPreference(getString(artificialKey1_strategySettings)).setOnPreferenceClickListener(
                new Preference.OnPreferenceClickListener() {
                    @Override
                    public boolean onPreferenceClick(Preference preference) {
                        Intent intent = new Intent(preference.getContext(), StrategySettingsActivity.class);
                        intent.putExtra(getString(strategyType), strategyList.getValue());
                        startActivity(intent);
                        return true;
                    }
                });

        findPreference(getString(language)).setOnPreferenceChangeListener(languageListListener);

        autoSetupNotification();
    }

    public void onResume() {
        super.onResume();
        updateSwitchState();
    }


    private void autoSetupNotification() {
        CommonNotificationDao dao = new CommonNotificationDao(getActivity());
        if (!dao.isNotificationRunning()) {
            return;
        }

        if (NotificationBroadcastReceiver.noPendingIntentExist(getActivity())) {
            ((MainSettingsActivity) getActivity()).initializeNotifications();
        }
    }

}
