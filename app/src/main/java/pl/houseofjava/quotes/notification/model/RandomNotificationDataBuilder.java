package pl.houseofjava.quotes.notification.model;

import org.joda.time.DateTime;

public class RandomNotificationDataBuilder {
    private DateTime dayStartHourMinute;
    private DateTime dayEndHourMinute;
    private DateTime periodStart;

    private DateTime lastNotificationTime;

    private Integer counter;
    private Integer currentCounter;

    private int minutesPeriod;

    public RandomNotificationDataBuilder withDayStartHourMinute(DateTime dayStartHourMinute) {
        this.dayStartHourMinute = dayStartHourMinute;
        return this;
    }

    public RandomNotificationDataBuilder withDayEndHourMinute(DateTime dayEndHourMinute) {
        this.dayEndHourMinute = dayEndHourMinute;
        return this;
    }

    public RandomNotificationDataBuilder withCounter(Integer counter) {
        this.counter = counter;
        return this;
    }

    public RandomNotificationDataBuilder withCurrentCounter(Integer currentCounter) {
        this.currentCounter = currentCounter;
        return this;
    }

    public RandomNotificationDataBuilder withLastNotificationTime(DateTime lastNotificationTime) {
        this.lastNotificationTime = lastNotificationTime;
        return this;
    }

    public RandomNotificationDataBuilder withPeriodStart(DateTime periodStart) {
        this.periodStart = periodStart;
        return this;
    }

    public RandomNotificationDataBuilder withMinutes(int minutes) {
        this.minutesPeriod = minutes;
        return this;
    }

    public RandomNotificationData build() {
        RandomNotificationData notificationData = new RandomNotificationData();
        notificationData.setCurrentCounter(currentCounter);
        notificationData.setCounter(counter);
        notificationData.setDayEndHourMinute(dayEndHourMinute);
        notificationData.setDayStartHourMinute(dayStartHourMinute);
        notificationData.setLastNotificationTime(lastNotificationTime);
        notificationData.setPeriodStart(periodStart);
        notificationData.setMinutesPeriod(minutesPeriod);

        return notificationData;
    }
}
