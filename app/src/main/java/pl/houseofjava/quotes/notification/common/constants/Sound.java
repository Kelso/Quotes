package pl.houseofjava.quotes.notification.common.constants;


import android.net.Uri;
import android.provider.Settings;

public enum Sound {

    Default(Settings.System.DEFAULT_NOTIFICATION_URI);

    public static final String Key = "notificationSound";

    private Uri uri;

    private Sound(Uri strUri) {
        this.uri = uri;
    }

    public Uri getUri() {
        return uri;
    }

    public String getStringUri() {
        return uri.getPath();
    }
}
