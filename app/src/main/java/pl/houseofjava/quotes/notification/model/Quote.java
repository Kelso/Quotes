package pl.houseofjava.quotes.notification.model;

import org.joda.time.Instant;

public class Quote {
    private int id;
    private int counter;
    private String body;
    private String origin;
    private Instant lastDisplayTimestamp;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public Instant getLastDisplayTimestamp() {
        return lastDisplayTimestamp;
    }

    public void setLastDisplayTimestamp(Instant lastDisplayTimestamp) {
        this.lastDisplayTimestamp = lastDisplayTimestamp;
    }

    public static class Builder {
        private int id;
        private int counter;
        private String body;
        private String origin;
        private Instant lastDisplayTimestamp;

        public Builder withCounter(int counter) {
            this.counter = counter;
            return this;
        }

        public Builder withId(int id) {
            this.id = id;
            return this;
        }

        public Builder withBody(String body) {
            this.body = body;
            return this;
        }

        public Builder withOrigin(String origin) {
            this.origin = origin;
            return this;
        }

        public Builder withLastDisplayedTimestamp(Integer timestampInSeconds) {
            this.lastDisplayTimestamp = timestampInSeconds != null ? new Instant(timestampInSeconds * 1000) : null;
            return this;
        }

        public Quote build() {
            Quote quote = new Quote();
            quote.setCounter(counter);
            quote.setBody(body);
            quote.setOrigin(origin);
            quote.setLastDisplayTimestamp(lastDisplayTimestamp);
            quote.setId(id);
            return quote;
        }
    }
}
