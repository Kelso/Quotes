package pl.houseofjava.quotes.notification.random;

import org.joda.time.DateTime;
import org.joda.time.Minutes;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import pl.houseofjava.quotes.notification.model.RandomNotificationData;

public class RandomNotificationUtil {
    private static int MIN_WINDOW_LENGTH_IN_MINUTES = 30;

    public static DateTime calculateNewNotificationTime(RandomNotificationData notificationData) {
        int randomMinutes = getRandomMinutesPeriod(notificationData.getMinutesPeriod());
        return notificationData.getPeriodStart().plusMinutes(randomMinutes);
    }

    private static int getRandomMinutesPeriod(int maxMinutesPeriod) {
        return new Random().nextInt(maxMinutesPeriod) + 1;
    }

    public static void calculateNewCounterAndPeriodLength(DateTime now, RandomNotificationData notificationData) {
        Window window = findProperWindow(now, notificationData);
        int minutes = 0;
        int newCounter = 0;
        DateTime periodStart = now;
        if (isDuringWindow(now, window)) {
            minutes = calculateMaxMinutesForPeriod(periodStart, window.getEnd(), notificationData.getCurrentCounter());
            newCounter = notificationData.getCurrentCounter() - 1;
        } else {
            periodStart = window.getStart();
            minutes = calculateMaxMinutesForPeriod(periodStart, window.getEnd(), notificationData.getCounter());
            newCounter = notificationData.getCounter() - 1;
        }

        notificationData.setPeriodStart(periodStart);
        notificationData.setMinutesPeriod(minutes);
        notificationData.setCurrentCounter(newCounter);
    }

    private static Window findProperWindow(DateTime now, RandomNotificationData notificationData) {
        List<Window> windows = prepareWindows(now, notificationData);
        Window properWindow = windows.get(windows.size() - 1);

        for (Window window : windows) {
            if (canNotify(now, window, notificationData)) {
                properWindow = window;
                break;
            }
        }
        return properWindow;
    }

    private static List<Window> prepareWindows(DateTime now, RandomNotificationData notificationData) {
        boolean windowDuringMidnight = notificationData.getDayStartHourMinute().getMillisOfDay() > notificationData.getDayEndHourMinute().getMillisOfDay();
        List<Window> windows = new ArrayList<>();
        int startingDayIndex = windowDuringMidnight ? -1 : 0;

        for (int i = startingDayIndex; i < 2; i++) {
            windows.add(new Window.Builder()
                    .setDuringMidnight(windowDuringMidnight)
                    .withStartHourMinute(notificationData.getDayStartHourMinute())
                    .withEndHourMinute(notificationData.getDayEndHourMinute())
                    .build(now.plusDays(i))
            );
        }
        return windows;
    }

    static boolean isDuringWindow(DateTime now, Window window) {
        return now.isAfter(window.getStart()) && now.isBefore(window.getEnd());
    }

    // MIN WINDOW = 30min
    // MAX COUNTER = 4
    public static boolean shouldAlarmOnce(int period) {
        return period < 30;
    }


    static boolean canNotify(DateTime now, Window window, RandomNotificationData notificationData) {
        int counter = notificationData.getCurrentCounter();
        if (counter == 0) {
            return false;
        }

        if (!isDuringWindow(now, window)) {
            counter = notificationData.getCounter();
        }

        return canNotify(now, window.getEnd(), counter);
    }

    static boolean canNotify(DateTime start, DateTime end, int counter) {
        return calculateMaxMinutesForPeriod(start, end, counter) >= MIN_WINDOW_LENGTH_IN_MINUTES;
    }

    static int calculateMaxMinutesForPeriod(DateTime now, DateTime endOfWindow, int currentCounter) {
        return Minutes.minutesBetween(now, endOfWindow).dividedBy(currentCounter).getMinutes();
    }
}


