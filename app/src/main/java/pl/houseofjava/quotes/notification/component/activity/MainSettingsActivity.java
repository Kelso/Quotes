package pl.houseofjava.quotes.notification.component.activity;

import android.app.Activity;
import android.os.Bundle;

import pl.houseofjava.quotes.notification.dao.preferences.CommonNotificationDao;
import pl.houseofjava.quotes.notification.dao.preferences.ConstantNotificationDao;
import pl.houseofjava.quotes.notification.dao.preferences.RandomNotificationDao;
import pl.houseofjava.quotes.notification.service.NotificationService;

import static pl.houseofjava.quotes.notification.component.broadcast.NotificationBroadcastReceiver.setupAlarm;

public class MainSettingsActivity extends Activity {
    public static final String PREFERENCES_NAME = "HouseOfJava_Quotes_Settings";

    public void initializeNotifications() {
        NotificationService notificationStrategy = new NotificationService.Builder().build(this);
        setupAlarm(this, notificationStrategy);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loadDefaultSettingsIfRequired();

        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new MainSettingsFragment())
                .commit();
    }

    private void loadDefaultSettingsIfRequired() {
        CommonNotificationDao commonSettings = new CommonNotificationDao(this);

        if (commonSettings.isNoStrategyLoaded()) {
            new ConstantNotificationDao(this).storeIntoPreferences();
            new RandomNotificationDao(this).storeIntoPreferences();
            commonSettings.storeIntoPreferences();
        }
    }

}
