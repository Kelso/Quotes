package pl.houseofjava.quotes.notification.service;

import org.joda.time.Instant;

import java.util.List;
import java.util.Random;

import pl.houseofjava.quotes.notification.dao.sql.QuoteDao;
import pl.houseofjava.quotes.notification.model.Quote;

public class QuoteService {

    private final QuoteDao quoteDao;

    public QuoteService(QuoteDao quoteDao) {
        this.quoteDao = quoteDao;
    }

    public Quote findRandomQuote() {
        List<Integer> ids = quoteDao.getIdsWithSmallestCount();
        int randomId = ids.get(new Random().nextInt(ids.size()));
        List<Quote> quotes = quoteDao.findQuotes(randomId, null, null);

        return quotes.get(0);
    }

    public void increaseCounterAndSetLastDisplayedTimestamp(Quote quote) {
        quoteDao.updateCounter(quote.getId(), quote.getCounter() + 1, (int) (new Instant().getMillis() / 1000));
    }

    public void close() {
        quoteDao.close();
    }
}
