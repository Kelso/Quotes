package pl.houseofjava.quotes.notification.component.activity;

import android.app.Activity;
import android.os.Bundle;
import android.preference.ListPreference;

import pl.houseofjava.quotes.R;
import pl.houseofjava.quotes.notification.common.constants.Strategy;
import pl.houseofjava.quotes.notification.dao.preferences.CommonNotificationDao;
import pl.houseofjava.quotes.notification.dao.preferences.ConstantNotificationDao;

import static android.widget.Toast.LENGTH_LONG;
import static android.widget.Toast.makeText;
import static pl.houseofjava.quotes.notification.common.constants.Strategy.Constant;

public class StrategySettingsActivity extends Activity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }

    public void onPause() {
        super.onPause();
        CommonNotificationDao dao = new CommonNotificationDao(this);
        if (!dao.isNotificationRunning()) {
            if (dao.getStrategy() == Strategy.Constant && !(new ConstantNotificationDao(this).anyNotificationEnabled())) {
                return;
            }
            makeText(this, getString(R.string.ui_toast_notification_start_reminder), LENGTH_LONG).show();
        }
    }

    public static class SettingsFragment extends CustomPreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            CommonNotificationDao commonNotificationDao = new CommonNotificationDao(getActivity());
            if (commonNotificationDao.getStrategy() == Constant) {
                addPreferencesFromResource(R.xml.strategy_constant_preferences);
            } else {
                addPreferencesFromResource(R.xml.strategy_random_preferences);
                addOnPreferenceChangeListener((ListPreference) findPreference(getString(R.string.random_counter)));
            }
        }
    }
}
