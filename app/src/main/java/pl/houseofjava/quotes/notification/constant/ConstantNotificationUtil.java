package pl.houseofjava.quotes.notification.constant;

import org.joda.time.DateTime;

import java.util.List;

import pl.houseofjava.quotes.notification.model.ConstantNotificationData;
import pl.houseofjava.quotes.notification.util.DateUtil;

public class ConstantNotificationUtil {
    public static void calculateNewNotificationTime(ConstantNotificationData notificationData, DateTime now) {
        Integer index = findIndex(notificationData.getNotificationTimes(), now);
        DateTime newNotificationTime = null;

        if (notificationData.isEnforceAlarmForNextDay() || index == null) {
            DateTime firstAlarm = notificationData.getFirstNotificationTime();
            newNotificationTime = new DateTime()
                    .withTimeAtStartOfDay()
                    .plusDays(1)
                    .withHourOfDay(firstAlarm.getHourOfDay())
                    .withMinuteOfHour(firstAlarm.getMinuteOfHour());
        } else {
            DateTime alarmTime = notificationData.getNotificationTimes().get(index);
            newNotificationTime = new DateTime()
                    .withTimeAtStartOfDay()
                    .withHourOfDay(alarmTime.getHourOfDay())
                    .withMinuteOfHour(alarmTime.getMinuteOfHour());
        }

        notificationData.setNewNotificationTime(newNotificationTime);
    }

    private static Integer findIndex(List<DateTime> dates, DateTime dateTime) {
        for (int i = 0; i < dates.size(); i++) {
            if (DateUtil.isAfterRegardingTime(dates.get(i), dateTime)) {
                return i;
            }
        }
        return null;
    }
}
