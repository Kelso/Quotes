package pl.houseofjava.quotes.notification.random.constants;

public enum MinutesToEndDay {

    Default(10);

    public static String KEY = "minutesToEndOfDay";
    private int value;

    MinutesToEndDay(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
