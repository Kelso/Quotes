package pl.houseofjava.quotes.notification.random.constants;

public enum Counter {

    Max(4),
    Min(1),
    Default(2);

    public static int NO_NOTIFICATIONS_FOR_TODAY_COUNTER_VALUE = 0;

    private Integer value;

    Counter(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }
}
