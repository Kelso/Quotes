package pl.houseofjava.quotes.notification.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TimePicker;

import org.joda.time.DateTime;

import pl.houseofjava.quotes.R;
import pl.houseofjava.quotes.notification.util.DateUtil;

import static android.text.format.DateFormat.is24HourFormat;
import static pl.houseofjava.quotes.notification.util.DateUtil.UNSET_NOTIFICATION;
import static pl.houseofjava.quotes.notification.util.DateUtil.getDateTime;
import static pl.houseofjava.quotes.notification.util.DateUtil.toHourMinute;

public class ConstantTimePreference extends DialogPreference {

    private DateTime calendar = null;
    private TimePicker picker = null;


    public ConstantTimePreference(Context ctx) {
        this(ctx, null);
    }

    public ConstantTimePreference(Context ctxt, AttributeSet attrs) {
        this(ctxt, attrs, android.R.attr.dialogPreferenceStyle);
    }

    public ConstantTimePreference(Context ctxt, AttributeSet attrs, int defStyle) {
        super(ctxt, attrs, defStyle);

        setPositiveButtonText(R.string.ui_settings_date_picker_set);
        setNegativeButtonText(R.string.ui_settings_date_picker_cancel);
    }

    @Override
    protected void onPrepareDialogBuilder(AlertDialog.Builder builder) {
        super.onPrepareDialogBuilder(builder);
        builder.setNeutralButton(getContext().getText(R.string.ui_constant_notyfication_hour_minute_unset), this);
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        super.onClick(dialog, which);

        switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
                int minutes = DateUtil.getMinutes(picker.getCurrentHour(), picker.getCurrentMinute());
                if (callChangeListener(minutes)) {
                    persistInt(minutes);
                    calendar= DateUtil.getDateTime(picker.getCurrentHour(), picker.getCurrentMinute());
                    notifyChanged();
                }
                break;

            case DialogInterface.BUTTON_NEUTRAL:
                if (callChangeListener(calendar.getMinuteOfDay())) {
                    persistInt(UNSET_NOTIFICATION);
                    calendar = null;
                    notifyChanged();
                }
                break;
        }
    }

    @Override
    protected View onCreateDialogView() {
        picker = new TimePicker(getContext());
        picker.setIs24HourView(is24HourFormat(getContext()));
        return picker;
    }

    @Override
    protected void onBindDialogView(View v) {
        super.onBindDialogView(v);
        if (calendar == null) {
            calendar = new DateTime();
        }
        picker.setCurrentHour(calendar.getHourOfDay());
        picker.setCurrentMinute(calendar.getMinuteOfHour());
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return (a.getString(index));
    }

    @Override
    protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {
        if (restoreValue) {
            calendar = getDateTime(getPersistedInt(UNSET_NOTIFICATION));
        } else {
            if (defaultValue == null) {
                calendar = DateTime.now();
            } else {
                calendar = getDateTime(getPersistedInt(UNSET_NOTIFICATION));
            }
        }
        setSummary(getSummary());
    }

    @Override
    public CharSequence getSummary() {
        if (calendar == null) {
            return null;
        }
        return toHourMinute(calendar);
    }
}

