package pl.houseofjava.quotes.notification.component.broadcast;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import pl.houseofjava.quotes.notification.ui.NotificationBuilder;
import pl.houseofjava.quotes.notification.util.logging.Loggable;

import static pl.houseofjava.quotes.notification.util.logging.Logger.logDebug;


public class CancelNotificationBroadcastReceiver extends BroadcastReceiver implements Loggable {
    public static final String CANCEL_NOTIFICATION_FORCE_NEXT_DAY = "CANCEL_NOTIFICATION_FORCE_NEXT_DAY";
    public static final String CANCEL_NOTIFICATION_NOT_FORCE_NEXT_DAY = "CANCEL_NOTIFICATION_NOT_FORCE_NEXT_DAY";

    public static PendingIntent getCancelPendingIntent(Context context) {
        Intent intent = new Intent(context, CancelNotificationBroadcastReceiver.class);
        intent.setAction(CANCEL_NOTIFICATION_NOT_FORCE_NEXT_DAY);
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
    }

    public static PendingIntent getCancelPendingIntentWithForceNextDay(Context context) {
        Intent intent = new Intent(context, CancelNotificationBroadcastReceiver.class);
        intent.setAction(CANCEL_NOTIFICATION_FORCE_NEXT_DAY);
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        logDebug(getLogTag(), "Cancelling notification.");
        manager.cancel(NotificationBuilder.TAG, NotificationBuilder.NOTIFICATION_ID);

        switch (intent.getAction()) {
            case CANCEL_NOTIFICATION_FORCE_NEXT_DAY:
                NotificationBroadcastReceiver.setupAlarm(context.getApplicationContext(), true);
                break;
            case CANCEL_NOTIFICATION_NOT_FORCE_NEXT_DAY:
                NotificationBroadcastReceiver.setupAlarm(context.getApplicationContext(), false);
                break;
            default:
                logDebug(getLogTag(), "Invalid intent.");
                break;
        }
    }

    @Override
    public String getLogTag() {
        return "CancelNotificationBR";
    }
}