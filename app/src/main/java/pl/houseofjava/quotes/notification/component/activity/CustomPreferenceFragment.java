package pl.houseofjava.quotes.notification.component.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;
import android.widget.Toast;

import pl.houseofjava.quotes.R;
import pl.houseofjava.quotes.notification.component.broadcast.NotificationBroadcastReceiver;
import pl.houseofjava.quotes.notification.dao.preferences.CommonNotificationDao;
import pl.houseofjava.quotes.notification.dao.preferences.ConstantNotificationDao;
import pl.houseofjava.quotes.notification.dao.preferences.RandomNotificationDao;
import pl.houseofjava.quotes.notification.util.NotificationException;
import pl.houseofjava.quotes.notification.util.Validator;
import pl.houseofjava.quotes.notification.util.logging.Loggable;

import static pl.houseofjava.quotes.notification.component.activity.MainSettingsActivity.PREFERENCES_NAME;
import static pl.houseofjava.quotes.notification.util.logging.Logger.logDebug;

public class CustomPreferenceFragment extends PreferenceFragment implements Loggable {

    private SharedPreferences.OnSharedPreferenceChangeListener preferenceChangeListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            CommonNotificationDao dao = new CommonNotificationDao(getActivity());

            if (!areAlarmsValid(dao) || key.equals(getString(R.string.language)) || (key.equals(getString(R.string.alarmEnable)) && dao.isNotificationRunning())) {
                return;
            }

            NotificationBroadcastReceiver.cancelAlarm(getActivity());
            dao.storeNotificationRunningState(false);
            updateSwitchState();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupPreferenceManager();
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(preferenceChangeListener);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(preferenceChangeListener);
    }

    protected void setupPreferenceManager() {
        PreferenceManager preferenceManager = getPreferenceManager();
        preferenceManager.setSharedPreferencesName(PREFERENCES_NAME);
        preferenceManager.setSharedPreferencesMode(Context.MODE_PRIVATE);
    }

    protected void addOnPreferenceChangeListener(final ListPreference listPreference) {
        listPreference.setOnPreferenceChangeListener(
                new Preference.OnPreferenceChangeListener() {
                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                        listPreference.setSummary(newValue.toString());
                        return true;
                    }
                });
    }

    protected void updateSwitchState() {
        final SwitchPreference preferenceScreen = (SwitchPreference) findPreference(getString(R.string.alarmEnable));
        if (preferenceScreen != null) {
            CommonNotificationDao settings = new CommonNotificationDao(getActivity());
            boolean notificationRunning = settings.isNotificationRunning();
            preferenceScreen.setChecked(notificationRunning);
            logDebug(getLogTag(), "Setting switch to %s.", notificationRunning);
        }
    }

    protected boolean areAlarmsValid(CommonNotificationDao commonNotificationDao) {
        boolean valid = true;
        try {
            validate(commonNotificationDao);
        } catch (NotificationException ne) {
            valid = false;
            Toast.makeText(getActivity(), getString(ne.getStringId()), Toast.LENGTH_LONG).show();
        }
        return valid;
    }

    private void validate(CommonNotificationDao commonNotificationDao) throws NotificationException {
        switch (commonNotificationDao.getStrategy()) {
            case Random:
                Validator.randomValidator.windowIsLongEnough(new RandomNotificationDao(getActivity()));
                break;
            case Constant:
                Validator.constantValidator.anyAlarmEnabled(new ConstantNotificationDao(getActivity()));
                break;
        }
    }

    public String getLogTag() {
        return "SharedPreferenceListener";
    }
}
