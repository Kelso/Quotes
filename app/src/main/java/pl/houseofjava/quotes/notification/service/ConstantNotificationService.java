package pl.houseofjava.quotes.notification.service;

import android.app.PendingIntent;
import android.content.Context;

import org.joda.time.DateTime;

import pl.houseofjava.quotes.notification.constant.ConstantNotificationUtil;
import pl.houseofjava.quotes.notification.dao.preferences.ConstantNotificationDao;
import pl.houseofjava.quotes.notification.model.ConstantNotificationData;
import pl.houseofjava.quotes.notification.util.logging.Loggable;
import pl.houseofjava.quotes.notification.util.logging.Logger;

import static pl.houseofjava.quotes.notification.component.broadcast.NotificationBroadcastReceiver.setAlarm;

public class ConstantNotificationService extends NotificationService implements Loggable {
    private final ConstantNotificationDao settings;

    public ConstantNotificationService(Context context) {
        this.settings = new ConstantNotificationDao(context);
    }

    @Override
    public void run(PendingIntent pendingIntent) {
        ConstantNotificationData notificationData = settings.getNotificationData(enforceAlarmForTomorrow);
        ConstantNotificationUtil.calculateNewNotificationTime(notificationData, new DateTime());

        setAlarm(settings.getContext(), notificationData.getNewNotificationTime(), pendingIntent);
        Logger.logDebug(getLogTag(), "Setting notification to: %s.", notificationData.getNewNotificationTime());
    }

    @Override
    public String getLogTag() {
        return "ConstantStrategy";
    }
}
