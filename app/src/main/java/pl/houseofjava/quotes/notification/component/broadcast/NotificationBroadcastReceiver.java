package pl.houseofjava.quotes.notification.component.broadcast;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.joda.time.DateTime;

import pl.houseofjava.quotes.notification.service.NotificationService;
import pl.houseofjava.quotes.notification.ui.NotificationBuilder;
import pl.houseofjava.quotes.notification.util.logging.Loggable;
import pl.houseofjava.quotes.notification.util.logging.Logger;

public class NotificationBroadcastReceiver extends BroadcastReceiver implements Loggable {
    public static final String SHOW_NOTIFICATION = "SHOW_NOTIFICATION";

    public static void setupAlarm(Context context, NotificationService notificationStrategy) {
        PendingIntent pi = getPendingIntentToShowNotification(context);
        notificationStrategy.run(pi);
    }

    public static void setupAlarm(Context context, boolean enforceNextDayAlarm) {
        NotificationService notificationStrategy = getNotificationStrategy(context, enforceNextDayAlarm);
        setupAlarm(context, notificationStrategy);
    }

    private static NotificationService getNotificationStrategy(Context context, boolean enforceNextDayAlarm) {
        NotificationService.Builder strategyBuilder = new NotificationService.Builder();
        if (enforceNextDayAlarm) {
            strategyBuilder.withEnforcingNextDayAlarm();
        }

        return strategyBuilder.build(context);
    }

    public static void cancelAlarm(Context context) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(NotificationBroadcastReceiver.getPendingIntentToShowNotification(context));
    }

    public static void setAlarm(Context context, DateTime date, PendingIntent pendingIntent) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, date.getMillis(), pendingIntent);
    }

    public static PendingIntent getPendingIntentToShowNotification(Context context) {
        Intent intent = prepareIntent(SHOW_NOTIFICATION, context);
        return preparePendingIntent(context, intent);
    }

    public static boolean noPendingIntentExist(Context context) {
        Intent intent = prepareIntent(SHOW_NOTIFICATION, context);
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_NO_CREATE) == null;
    }

    private static PendingIntent preparePendingIntent(Context context, Intent intent) {
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private static Intent prepareIntent(String action, Context context) {
        return new Intent(action, null, context, NotificationBroadcastReceiver.class);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Logger.logDebug(getLogTag(), "Message received: %s.", intent.getAction());
        showNotification(context);
    }

    private void showNotification(Context context) {
        NotificationBuilder notificationBuilder = new NotificationBuilder(context);
        Logger.logDebug(getLogTag(), "Displaying notification.");
        notificationBuilder.buildNotificationAndNotify();

    }

    @Override
    public String getLogTag() {
        return "BroadcastReceiver";
    }
}
