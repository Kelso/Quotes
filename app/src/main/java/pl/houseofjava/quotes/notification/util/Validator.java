package pl.houseofjava.quotes.notification.util;

import pl.houseofjava.quotes.notification.constant.ConstantValidator;
import pl.houseofjava.quotes.notification.random.RandomValidator;

public class Validator {
    public static final RandomValidator randomValidator = new RandomValidator();
    public static final ConstantValidator constantValidator = new ConstantValidator();

}
