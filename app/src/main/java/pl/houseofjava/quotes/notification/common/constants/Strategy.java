package pl.houseofjava.quotes.notification.common.constants;

import pl.houseofjava.quotes.notification.service.NotificationService;
import pl.houseofjava.quotes.notification.service.ConstantNotificationService;
import pl.houseofjava.quotes.notification.service.RandomNotificationService;

public enum Strategy {

    Random("0", RandomNotificationService.class),
    Constant("1", ConstantNotificationService.class);

    private String value;
    private Class<? extends NotificationService> clazz;

    Strategy(String value, Class<? extends NotificationService> clazz) {
        this.clazz = clazz;
        this.value = value;
    }

    Strategy(Strategy defaultStrategy) {
        this.value = defaultStrategy.value;
        this.clazz = defaultStrategy.clazz;
    }

    public static Strategy valueOfString(String value) {
        for (Strategy strategy : Strategy.values()) {
            if (strategy.value.equals(value)) {
                return strategy;
            }
        }
        return Random;
    }

    public Class<? extends NotificationService> getStrategyClazz() {
        return clazz;
    }

    public static Class<? extends NotificationService> getStrategyClass(String value) {
        for (Strategy strategy : Strategy.values()) {
            if (strategy.value.equals(value)) {
                return strategy.clazz;
            }
        }
        return Random.clazz;
    }

    public String getValue() {
        return value;
    }

}
