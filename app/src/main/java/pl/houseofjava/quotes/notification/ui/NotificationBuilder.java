package pl.houseofjava.quotes.notification.ui;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

import pl.houseofjava.quotes.R;
import pl.houseofjava.quotes.notification.dao.preferences.CommonNotificationDao;
import pl.houseofjava.quotes.notification.dao.sql.QuoteDao;
import pl.houseofjava.quotes.notification.model.Quote;
import pl.houseofjava.quotes.notification.service.QuoteService;

import static pl.houseofjava.quotes.notification.component.broadcast.CancelNotificationBroadcastReceiver.getCancelPendingIntent;
import static pl.houseofjava.quotes.notification.component.broadcast.CancelNotificationBroadcastReceiver.getCancelPendingIntentWithForceNextDay;

public class NotificationBuilder {
    public static final String TAG = "HOUSEOJ_QUOTES_NOTIFICATION";
    public static final int NOTIFICATION_ID = 101;
    private final Context context;
    private final NotificationManager notificationManager;
    private final QuoteService quoteService;

    public NotificationBuilder(Context context) {
        this.context = context;
        this.notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        CommonNotificationDao commonNotificationDao = new CommonNotificationDao(context);
        String language = commonNotificationDao.getLanguage();

        this.quoteService = new QuoteService(new QuoteDao(context, language));
    }

    public void buildNotificationAndNotify() {
        Quote quote = quoteService.findRandomQuote();
        Notification notification = buildNotification(quote);
        notificationManager.notify(TAG, NOTIFICATION_ID, notification);
        quoteService.increaseCounterAndSetLastDisplayedTimestamp(quote);
        quoteService.close();
    }

    private Notification buildNotification(Quote quote) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setContentTitle(quote.getOrigin())
                .setContentText(quote.getBody())
                .setSmallIcon(R.drawable.ic_launcher)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(quote.getBody()))
                .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE)
                .setDeleteIntent(getCancelPendingIntent(context))
                .addAction(R.drawable.ic_dismiss, context.getString(R.string.notification_dismiss), getCancelPendingIntentWithForceNextDay(context))
                .addAction(R.drawable.ic_next, context.getString(R.string.notification_next), getCancelPendingIntent(context));


        //ToDo test purposes
//        builder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
//        builder.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});

        return builder.build();
    }

    private String getQuote() {
        return "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum";
    }
}

