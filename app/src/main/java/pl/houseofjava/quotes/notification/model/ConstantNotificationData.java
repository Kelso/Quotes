package pl.houseofjava.quotes.notification.model;

import org.joda.time.DateTime;

import java.util.List;

public class ConstantNotificationData {
    private List<DateTime> notificationTimes;
    private boolean enforceAlarmForNextDay;
    private DateTime newNotificationTime;

    public ConstantNotificationData(boolean enforceAlarmForNextDay, List<DateTime> dateTimes) {
        this.notificationTimes = dateTimes;
        this.enforceAlarmForNextDay = enforceAlarmForNextDay;
    }

    public List<DateTime> getNotificationTimes() {
        return notificationTimes;
    }

    public boolean isEnforceAlarmForNextDay() {
        return enforceAlarmForNextDay;
    }

    public DateTime getLastNotificationTime() {
        return notificationTimes.get(notificationTimes.size() - 1);
    }

    public DateTime getFirstNotificationTime() {
        return notificationTimes.get(0);
    }

    public DateTime getNotificationTime(int index) {
        return notificationTimes.get(index);
    }

    public DateTime getNewNotificationTime() {
        return newNotificationTime;
    }

    public void setNewNotificationTime(DateTime newNotificationTime) {
        this.newNotificationTime = newNotificationTime;
    }
}
