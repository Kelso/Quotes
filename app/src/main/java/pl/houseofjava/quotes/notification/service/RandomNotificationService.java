package pl.houseofjava.quotes.notification.service;

import android.app.PendingIntent;
import android.content.Context;

import org.joda.time.DateTime;

import pl.houseofjava.quotes.notification.dao.preferences.RandomNotificationDao;
import pl.houseofjava.quotes.notification.model.RandomNotificationData;
import pl.houseofjava.quotes.notification.random.constants.Counter;
import pl.houseofjava.quotes.notification.util.logging.Loggable;
import pl.houseofjava.quotes.notification.util.logging.Logger;

import static org.joda.time.DateTime.now;
import static pl.houseofjava.quotes.notification.component.broadcast.NotificationBroadcastReceiver.setAlarm;
import static pl.houseofjava.quotes.notification.random.RandomNotificationUtil.calculateNewCounterAndPeriodLength;
import static pl.houseofjava.quotes.notification.random.RandomNotificationUtil.calculateNewNotificationTime;


public class RandomNotificationService extends NotificationService implements Loggable {
    private final RandomNotificationDao settings;

    public RandomNotificationService(Context context) {
        this.settings = new RandomNotificationDao(context);
    }

    @Override
    public void run(PendingIntent pendingIntent) {
        if (enforceAlarmForTomorrow) {
            settings.setCurrentCounter(Counter.NO_NOTIFICATIONS_FOR_TODAY_COUNTER_VALUE);
        }

        RandomNotificationData notificationData = settings.getRandomNotificationData();
        calculateNewCounterAndPeriodLength(now(), notificationData);
        DateTime newNotificationTime = calculateNewNotificationTime(notificationData);

        settings.setCurrentCounter(notificationData.getCurrentCounter());

        setAlarm(settings.getContext(), newNotificationTime, pendingIntent);
        Logger.logDebug(getLogTag(), "Setting notification to %s. Number of next alarms to set: %s.", newNotificationTime, settings.getCurrentCounter());
    }

    @Override
    public String getLogTag() {
        return "RandomStrategy";
    }
}
