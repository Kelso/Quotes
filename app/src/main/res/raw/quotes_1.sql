INSERT INTO quote (origin) VALUES ('Jeremiah 29:11');
INSERT INTO quote_body_pol (body, quote_id) VALUES('Jakis cytat...', (SELECT _id FROM quote WHERE origin = 'Jeremiah 29:11'));
INSERT INTO quote_body_eng (body, quote_id) VALUES('Some quote...', (SELECT _id FROM quote WHERE origin = 'Jeremiah 29:11'));
INSERT INTO quote_body_esp (body, quote_id) VALUES('Quotea...', (SELECT _id FROM quote WHERE origin = 'Jeremiah 29:11'));

INSERT INTO quote (origin) VALUES ('Matthew 29:11');
INSERT INTO quote_body_pol (body, quote_id) VALUES('Już siekiera do korzenia drzew jest przyłożona. Każde więc drzewo, które nie wydaje dobrego owocu, będzie wycięte i w ogień wrzucone.', (SELECT _id FROM quote WHERE origin = 'Jeremiah 29:11'));
INSERT INTO quote_body_eng (body, quote_id) VALUES('Even now the ax lies at the root of the trees. Therefore every tree that does not bear good fruit will be cut down and thrown into the fire.', (SELECT _id FROM quote WHERE origin = 'Jeremiah 29:11'));
INSERT INTO quote_body_esp (body, quote_id) VALUES('Quotea...', (SELECT _id FROM quote WHERE origin = 'Jeremiah 29:11'));
