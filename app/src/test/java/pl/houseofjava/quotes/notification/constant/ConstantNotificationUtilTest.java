package pl.houseofjava.quotes.notification.constant;

import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import pl.houseofjava.quotes.notification.assertions.DateAssert;
import pl.houseofjava.quotes.notification.model.ConstantNotificationData;

@RunWith(MockitoJUnitRunner.class)
public class ConstantNotificationUtilTest {

    List<DateTime> dateTimes = Arrays.asList(
            new DateTime().withTimeAtStartOfDay().withHourOfDay(9).withMinuteOfHour(30),
            new DateTime().withTimeAtStartOfDay().withHourOfDay(15).withMinuteOfHour(0));


    @Test
    public void shouldSetAlarmForTomorrow() {
        // given
        ConstantNotificationData constantNotificationData = new ConstantNotificationData(false, dateTimes);
        DateTime dateTime = new DateTime().withHourOfDay(15).withMinuteOfHour(1);

        // when
        ConstantNotificationUtil.calculateNewNotificationTime(constantNotificationData, dateTime);

        // then
        DateAssert.assertThat(constantNotificationData.getNewNotificationTime())
                .isNextDay()
                .isEqual(new DateTime().plusDays(1).withTimeAtStartOfDay().withHourOfDay(9).withMinuteOfHour(30));
    }

    @Test
    public void shouldSetAlarmForTomorrowWhenForced() {
        // given
        ConstantNotificationData constantNotificationData = new ConstantNotificationData(true, dateTimes);
        DateTime dateTime = new DateTime().withHourOfDay(14).withMinuteOfHour(1);

        // when
        ConstantNotificationUtil.calculateNewNotificationTime(constantNotificationData, dateTime);

        // then
        DateAssert.assertThat(constantNotificationData.getNewNotificationTime())
                .isNextDay()
                .isEqual(new DateTime().plusDays(1).withTimeAtStartOfDay().withHourOfDay(9).withMinuteOfHour(30));
    }

    @Test
    public void shouldSetAlarmForToday() {
        // given
        DateTime dateTime = new DateTime().withHourOfDay(14).withMinuteOfHour(1);
        ConstantNotificationData constantNotificationData = new ConstantNotificationData(false, dateTimes);

        // when
        ConstantNotificationUtil.calculateNewNotificationTime(constantNotificationData, dateTime);

        // then
        DateAssert.assertThat(constantNotificationData.getNewNotificationTime())
                .isEqual(new DateTime().withTimeAtStartOfDay().withHourOfDay(15).withMinuteOfHour(0));
    }

    @Test
    public void shouldSetAlarmForToday2() {
        // given
        DateTime dateTime = new DateTime().withHourOfDay(6).withMinuteOfHour(1);
        ConstantNotificationData constantNotificationData = new ConstantNotificationData(false, dateTimes);

        // when
        ConstantNotificationUtil.calculateNewNotificationTime(constantNotificationData, dateTime);

        // then
        DateAssert.assertThat(constantNotificationData.getNewNotificationTime())
                .isEqual(new DateTime().withTimeAtStartOfDay().withHourOfDay(9).withMinuteOfHour(30));
    }
}