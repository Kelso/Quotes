package pl.houseofjava.quotes.notification.random;

import android.content.Context;

import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import pl.houseofjava.quotes.notification.dao.preferences.RandomNotificationDao;

@RunWith(MockitoJUnitRunner.class)
public class RandomNotificationDataTest {

    @Mock
    RandomNotificationDao settings;

    @Mock
    Context context;

//    @Before
//    public void before() {
//        when(settings.getDayStartHourMinute()).thenReturn(parse("9:00"));
//        when(settings.getDayEndHourMinute()).thenReturn(parse("23:00"));
//        when(settings.getCurrentCounter()).thenReturn(2);
//        when(settings.getCounter()).thenReturn(4);
//    }
//
//    @Test
//    public void shouldCreateAlarmNotificationDataWhenCurrentHourIsBetweenStartAndEndDayHour() {
//        // given
//        DateTime calendar = new DateTime().withHourOfDay(10).withMinuteOfHour(29);
//
//        // when
//        RandomNotificationData actual = new RandomNotificationData();
//
//        // then
//        new RandomAlarmRequestDataAssert().assertThat(actual)
//                .hasCurrentCounter(2)
//                .hasStartDayHour(10)
//                .hasMinutesPeriod(((23 - 10) * 60 - 29) / 3);
//    }
//
//    @Test
//    public void shouldCreateAlarmRequestDataWhenCurrentHourIsBeyondPossibleHours() {
//        // given
//        DateTime calendar = new DateTime().withHourOfDay(1).withMinuteOfHour(29);
//
//        // when
//        RandomNotificationData actual = new RandomNotificationData();
//
//        // then
//        new RandomAlarmRequestDataAssert().assertThat(actual)
//                .hasStartPeriodHour(9)
//                .hasMinutes(0)
//                .hasSeconds(0)
//                .hasCurrentCounter(-1);
//    }
}