package pl.houseofjava.quotes.notification.dao.sql;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class QuoteSQLiteHelperTest {

    @Test
    public void shouldPrepareProperWhereClause() {
        // given
        StringBuilder stringBuilder = new StringBuilder();

        // when
        QuoteSQLiteHelper.prepareWhereClause(stringBuilder, new ArrayList<String>(), 2, 10, "ok");

        // then
        Assert.assertEquals("WHERE q._id=? AND q.counter=? AND q.origin=? ", stringBuilder.toString());


        // given
        stringBuilder = new StringBuilder();

        // when
        QuoteSQLiteHelper.prepareWhereClause(stringBuilder, new ArrayList<String>(), null, 10, "ok");

        // then
        Assert.assertEquals("WHERE q.counter=? AND q.origin=? ", stringBuilder.toString());


        // given
        stringBuilder = new StringBuilder();

        // when
        QuoteSQLiteHelper.prepareWhereClause(stringBuilder, new ArrayList<String>(), null, null, null);

        // then
        Assert.assertEquals("", stringBuilder.toString());
    }
}