package pl.houseofjava.quotes.notification.random;

import junit.framework.Assert;

import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import pl.houseofjava.quotes.notification.assertions.DateAssert;
import pl.houseofjava.quotes.notification.assertions.RandomNotificationDataAssert;
import pl.houseofjava.quotes.notification.model.RandomNotificationData;
import pl.houseofjava.quotes.notification.model.RandomNotificationDataBuilder;

import static org.joda.time.DateTime.now;
import static pl.houseofjava.quotes.notification.random.RandomNotificationUtil.calculateNewCounterAndPeriodLength;

@RunWith(MockitoJUnitRunner.class)
public class RandomNotificationUtilTest {

    @Test
    public void shouldReturnProperBoolean() {
        // given
        Window window = new Window.Builder()
                .withStartHourMinute(getDateTime(9, 0))
                .withEndHourMinute(getDateTime(21, 0))
                .build(now());

        DateTime now = getDateTime(8, 59);

        // when
        boolean duringWindow = RandomNotificationUtil.isDuringWindow(now, window);

        // then
        Assert.assertEquals(false, duringWindow);


        //given
        now = getDateTime(20, 59);

        // when
        duringWindow = RandomNotificationUtil.isDuringWindow(now, window);

        // then
        Assert.assertEquals(true, duringWindow);


        //given
        now = getDateTime(21, 0);

        // when
        duringWindow = RandomNotificationUtil.isDuringWindow(now, window);

        // then
        Assert.assertEquals(false, duringWindow);
    }

    @Test
    public void shouldReturnDataForNextWindowWhenWindowIsThroughMidnight() {
        // given
        RandomNotificationData data = new RandomNotificationDataBuilder()
                .withCounter(3)
                .withCurrentCounter(2)
                .withDayStartHourMinute(getDateTime(21, 0))
                .withDayEndHourMinute(getDateTime(3, 0))
                .build();

        DateTime now = getDateTime(2, 50);

        // when
        calculateNewCounterAndPeriodLength(now, data);

        // then
        RandomNotificationDataAssert.assertThat(data)
                .hasMinutesPeriod(120)
                .hasNewCounter(3 - 1)
                .hasPeriodStart(getDateTime(21, 0));
    }

    @Test
    public void shouldReturnDataForNextDayWindowWhenWindowIsThroughMidnight() {
        // given
        RandomNotificationData data = new RandomNotificationDataBuilder()
                .withCounter(3)
                .withCurrentCounter(2)
                .withDayStartHourMinute(getDateTime(21, 0))
                .withDayEndHourMinute(getDateTime(0, 1))
                .build();

        DateTime now = getDateTime(23, 55);

        // when
        calculateNewCounterAndPeriodLength(now, data);

        // then
        RandomNotificationDataAssert.assertThat(data)
                .hasMinutesPeriod(60)
                .hasNewCounter(3 - 1)
                .hasPeriodStart(getDateTime(21, 0).plusDays(1));
    }

    @Test
    public void shouldReturnDataForNextDayWindow() {
        // given
        RandomNotificationData data = new RandomNotificationDataBuilder()
                .withCounter(3)
                .withCurrentCounter(2)
                .withDayStartHourMinute(getDateTime(9, 0))
                .withDayEndHourMinute(getDateTime(21, 0))
                .build();

        DateTime now = getDateTime(21, 2);

        // when
        calculateNewCounterAndPeriodLength(now, data);

        // then
        RandomNotificationDataAssert.assertThat(data)
                .hasMinutesPeriod(240)
                .hasNewCounter(3 - 1)
                .hasPeriodStart(getDateTime(9, 0).plusDays(1));

        // given
        now = getDateTime(20, 1);

        // when
        calculateNewCounterAndPeriodLength(now, data);

        // then
        RandomNotificationDataAssert.assertThat(data)
                .hasMinutesPeriod(240)
                .hasNewCounter(3 - 1)
                .hasPeriodStart(getDateTime(9, 0).plusDays(1));
    }

    @Test
    public void shouldReturnDataForTodayWindow() {
        // given
        RandomNotificationData data = new RandomNotificationDataBuilder()
                .withCounter(3)
                .withCurrentCounter(2)
                .withDayStartHourMinute(getDateTime(9, 0))
                .withDayEndHourMinute(getDateTime(21, 0))
                .build();

        DateTime now = getDateTime(19, 0);

        // when
        calculateNewCounterAndPeriodLength(now, data);

        // then
        RandomNotificationDataAssert.assertThat(data)
                .hasMinutesPeriod(60)
                .hasNewCounter(2 - 1)
                .hasPeriodStart(now);

        // given
        now = getDateTime(8, 59);

        // when
        calculateNewCounterAndPeriodLength(now, data);

        // then
        RandomNotificationDataAssert.assertThat(data)
                .hasMinutesPeriod(240)
                .hasNewCounter(3 - 1)
                .hasPeriodStart(getDateTime(9, 0));
    }

    @Test
    public void shouldCalculateMinutesForPeriod() {
        // given
        DateTime now = getDateTime(9, 0);
        DateTime endOfWindow = getDateTime(21, 1);
        int counter = 4;

        // when
        int minutes = RandomNotificationUtil.calculateMaxMinutesForPeriod(now, endOfWindow, counter);

        // then
        Assert.assertEquals(12 * 60 / 4, minutes);
    }

    @Test
    public void shouldAllowNotification() {
        DateTime now = getDateTime(9, 0);

        RandomNotificationData data = new RandomNotificationDataBuilder()
                .withCounter(3)
                .withCurrentCounter(2)
                .build();

        Window window = new Window.Builder()
                .withStartHourMinute(getDateTime(9, 0))
                .withEndHourMinute(getDateTime(21, 0))
                .build(now);


        boolean canNotify = RandomNotificationUtil.canNotify(now, window, data);

        Assert.assertEquals(true, canNotify);
    }

    @Test
    public void shouldCalculateNewNotificationTime() {
        // given
        DateTime now = now();

        RandomNotificationData data = new RandomNotificationDataBuilder()
                .withCounter(3)
                .withCurrentCounter(2)
                .withDayStartHourMinute(getDateTime(9, 0))
                .withDayEndHourMinute(getDateTime(21, 0))
                .withMinutes(55)
                .withPeriodStart(now)
                .build();

        // when
        DateTime newNotificationTime = RandomNotificationUtil.calculateNewNotificationTime(data);

        // then
        DateAssert.assertThat(newNotificationTime).isBeforeOrEqual(now.plusMinutes(55));
    }


//
//    @Test
//    public void shouldAllowOnlyOneAlarm() {
//        // given
//        DateTime calendar = new DateTime().withHourOfDay(21).withMinuteOfHour(10);
//        RandomNotificationData notificationData = settings.getRandomNotificationData();
//
//        // when
//        calculateNewCounterAndPeriodLength(notificationData);
//
//        // then
//        new DateAssert().assertThat(notificationData.getNewNotificationTime())
//                .isBeforeOrEqual(new DateTime().withHourOfDay(23).withMinuteOfHour(0))
//                .isAfterOrEqual(new DateTime().withHourOfDay(20).withMinuteOfHour(40));
//        new RandomAlarmDataAssert().assertThat(notificationData).hasNewCounter(0);
//    }

    private DateTime getDateTime(int hourOfDay, int minuteOfHour) {
        return new DateTime().withHourOfDay(hourOfDay).withMinuteOfHour(minuteOfHour).withSecondOfMinute(0).withMillisOfSecond(0);
    }
}