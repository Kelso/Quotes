package pl.houseofjava.quotes.notification.assertions;

import junit.framework.Assert;

import org.joda.time.DateTime;

import pl.houseofjava.quotes.notification.model.RandomNotificationData;


public class RandomNotificationDataAssert {
    private final RandomNotificationData randomNotificationData;

    private RandomNotificationDataAssert(RandomNotificationData actual) {
        this.randomNotificationData = actual;
    }

    public static RandomNotificationDataAssert assertThat(RandomNotificationData actual) {
        return new RandomNotificationDataAssert(actual);
    }

    public RandomNotificationDataAssert hasNewCounter(int counter) {
        Assert.assertEquals((Integer) counter, (Integer) randomNotificationData.getCurrentCounter());
        return this;
    }

    public RandomNotificationDataAssert hasMinutesPeriod(int minutes) {
        Assert.assertEquals((Integer) minutes, randomNotificationData.getMinutesPeriod());
        return this;
    }

    public RandomNotificationDataAssert hasPeriodStart(DateTime dateTime) {
        Assert.assertEquals(dateTime, randomNotificationData.getPeriodStart());
        return this;
    }

}
