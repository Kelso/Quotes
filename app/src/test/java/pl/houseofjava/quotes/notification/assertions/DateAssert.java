package pl.houseofjava.quotes.notification.assertions;

import junit.framework.Assert;

import org.joda.time.DateTime;

public class DateAssert {
    private DateTime actual = new DateTime();

    private DateAssert(DateTime actual) {
        this.actual = actual;
    }

    public static DateAssert assertThat(DateTime date) {
        return new DateAssert(date);
    }

    public DateAssert isNextDay() {
        Assert.assertEquals(true, new DateTime().plusDays(1).getDayOfYear() == actual.getDayOfYear());
        return this;
    }

    public DateAssert isEqual(DateTime givenTime) {
        Assert.assertEquals(givenTime, actual);
        return this;
    }

    public DateAssert isBeforeOrEqual(DateTime givenDate) {
        Assert.assertEquals(true, actual.isBefore(givenDate) || actual.isEqual(givenDate));
        return this;
    }

    public DateAssert isAfterOrEqual(DateTime givenDate) {
        Assert.assertEquals(true, actual.isAfter(givenDate) || actual.isEqual(givenDate));
        return this;
    }

}
