package pl.houseofjava.quotes;

import android.content.SharedPreferences;

import pl.houseofjava.quotes.notification.common.constants.Strategy;
import pl.houseofjava.quotes.notification.random.constants.MinutesToEndDay;

public class CurrentSettingsTestBuilder {
    private SharedPreferences.Editor editor;
    private int startPeriodHour;
    private int endDayHour;
    private int dayFrequency;
    private Strategy strategy;
    private int minutesToEndOfDay;

    private CurrentSettingsTestBuilder(Builder builder) {
        this.editor = builder.editor;
        this.startPeriodHour = builder.startPeriodHour;
        this.endDayHour = builder.endDayHour;
        this.dayFrequency = builder.dayFrequency;
        this.strategy = builder.strategy;
        this.minutesToEndOfDay = builder.minutesToEndOfDay;
    }

    public void apply() {
        editor.apply();
    }

    public int getStartPeriodHour() {
        return startPeriodHour;
    }

    public int getEndDayHour() {
        return endDayHour;
    }

    public int getDayFrequency() {
        return dayFrequency;
    }

    public Strategy getStrategy() {
        return strategy;
    }

    public int getMinutesToEndOfDay() {
        return minutesToEndOfDay;
    }

    public SharedPreferences.Editor getEditor() {
        return editor;
    }

    public static class Builder {
        private int startPeriodHour;
        private int endDayHour;
        private int dayFrequency;
        private Strategy strategy;
        private int minutesToEndOfDay;
        private SharedPreferences.Editor editor;

        public Builder(SharedPreferences.Editor editor) {
            this.editor = editor;
        }

        public Builder withStartPeriodHour(int hour) {
            this.startPeriodHour = hour;
            return this;
        }

        public Builder withEndDayHour(int hour) {
            this.endDayHour = hour;
            return this;
        }

        public Builder withFrequency(int frequency) {
            this.dayFrequency = frequency;
            return this;
        }

        public Builder withStrategy(Strategy strategy) {
            this.strategy = strategy;
            return this;
        }

        public Builder withMinutesToEndOfDay(int minutes) {
            this.minutesToEndOfDay = minutes;
            editor.putInt(MinutesToEndDay.KEY, minutes);
            return this;
        }

        public CurrentSettingsTestBuilder create() {
            return new CurrentSettingsTestBuilder(this);
        }
    }


}
