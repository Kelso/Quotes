package pl.houseofjava.quotes.notification;


import android.content.Context;
import android.content.SharedPreferences;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.SdkSuppress;
//import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
//import android.support.test.uiautomator.UiDevice;
import android.test.suitebuilder.annotation.LargeTest;

//import org.junit.Before;
//import org.junit.Rule;
//import org.junit.Test;
import org.junit.runner.RunWith;

//import java.util.Calendar;
//
//import pl.houseofjava.quotes.CurrentSettingsTestBuilder;
//import pl.houseofjava.quotes.R;
//import pl.houseofjava.quotes.notification.activity.SettingsActivity;
//import pl.houseofjava.quotes.notification.common.constants.Strategy;
//
//import static android.support.test.espresso.Espresso.onView;
//import static android.support.test.espresso.action.ViewActions.click;
//import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
@SdkSuppress(minSdkVersion = 18)
@LargeTest
public class SettingsTest {

//    @Rule
//    public ActivityTestRule<SettingsActivity> settingsActivity = new ActivityTestRule<SettingsActivity>(SettingsActivity.class);
//
//    private UiDevice mDevice;
//
//    @Before
//    public void setUp() throws Exception {
//        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
//    }
//
//    @Test
//    public void testActivity() {
//        SettingsActivity settings = settingsActivity.getActivity();
//        Calendar calendar = Calendar.getInstance();
//
//        int currentHour = calendar.get(Calendar.HOUR);
//        int currentMinute = calendar.get(Calendar.MINUTE);
//
//        SharedPreferences preferences = settings.getPreferences(Context.MODE_PRIVATE);
//
//        CurrentSettingsTestBuilder currentSettings = new CurrentSettingsTestBuilder.Builder(preferences.edit())
//                .withStartPeriodHour(currentHour)
//                .withEndDayHour(currentHour + 1)
//                .withFrequency(1)
//                .withMinutesToEndOfDay(60 - currentMinute)
//                .withStrategy(Strategy.Random)
//                .create();
//
//        currentSettings.apply();
//
//        onView(withId(R.id.initNotificationBtn)).perform(click());
//
//
//        mDevice.openNotification();
//        mDevice.wait(Until.hasObject(By.pkg("com.android.systemui")), 50000);
//    }
}
