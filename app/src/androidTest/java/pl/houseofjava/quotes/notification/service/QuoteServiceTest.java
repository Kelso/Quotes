package pl.houseofjava.quotes.notification.service;

import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import pl.houseofjava.quotes.notification.dao.sql.QuoteDao;
import pl.houseofjava.quotes.notification.dao.sql.QuoteSQLiteHelper;
import pl.houseofjava.quotes.notification.model.Quote;
import pl.houseofjava.quotes.notification.util.QuoteAssert;

import static android.support.test.InstrumentationRegistry.getTargetContext;

@RunWith(AndroidJUnit4.class)
public class QuoteServiceTest {


    private QuoteService quoteService;

    private QuoteDao quoteDao;

    @Before
    public void setUp() throws Exception {
        getTargetContext().deleteDatabase(QuoteSQLiteHelper.DATABASE_NAME);
        quoteDao = new QuoteDao(getTargetContext(), "pol");
        quoteDao.clearQuotes();

        quoteService = new QuoteService(quoteDao);
    }

    @After
    public void tearDown() throws Exception {
        quoteService.close();
    }

    @Test
    public void findRandomQuote() {
        // when
        quoteDao.insertQuote(new Quote.Builder().withOrigin("who1").withBody("body1").withCounter(0).build());
        quoteDao.insertQuote(new Quote.Builder().withOrigin("who2").withBody("body2").withCounter(1).build());
        quoteDao.insertQuote(new Quote.Builder().withOrigin("who3").withBody("body3").withCounter(1).build());

        Quote quote = quoteService.findRandomQuote();

        // then
        QuoteAssert.assertThat(quote)
                .hasBody("body1")
                .hasCounter(0)
                .hasOrigin("who1");
    }
}