package pl.houseofjava.quotes.notification.dao.sql;

import android.support.test.runner.AndroidJUnit4;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import pl.houseofjava.quotes.notification.model.Quote;
import pl.houseofjava.quotes.notification.util.QuoteAssert;

import static android.support.test.InstrumentationRegistry.getTargetContext;

@RunWith(AndroidJUnit4.class)
public class QuoteDaoTest {

    public static final String LANGUAGE = "pol";
    private QuoteDao quoteDao;

    @Before
    public void setUp() throws Exception {
        getTargetContext().deleteDatabase(QuoteSQLiteHelper.DATABASE_NAME);
        quoteDao = new QuoteDao(getTargetContext(), LANGUAGE);
        quoteDao.clearQuotes();
    }

    @After
    public void tearDown() throws Exception {
        quoteDao.close();
    }

    @Test
    public void shouldGetIdsWithSmallestCount() throws Exception {
        // given
        quoteDao.insertQuote(new Quote.Builder().withCounter(1).withBody("abcd").withOrigin("who").build());
        quoteDao.insertQuote(new Quote.Builder().withCounter(2).withBody("abcd").withOrigin("who2").build());

        // when
        List<Integer> ids = quoteDao.getIdsWithSmallestCount();

        // then
        Assert.assertEquals(ids.size(), 1);
        Quote quote = quoteDao.findQuotes(null, null, "who").get(0);
        Assert.assertEquals(Integer.valueOf(quote.getId()), ids.get(0));
    }

    @Test
    public void shouldUpdateCounter() throws Exception {
        // given
        quoteDao.insertQuote(new Quote.Builder().withCounter(1).withBody("abcd").withOrigin("who").build());
        Quote quote = quoteDao.findQuotes(null, null, "who").get(0);

        // when
        quoteDao.updateCounter(quote.getId(), 2, 0);

        // then
        quote = quoteDao.findQuotes(quote.getId(), null, null).get(0);
        Assert.assertEquals(2, quote.getCounter());
    }

    @Test
    public void shouldClearQuotes() {
        // given
        quoteDao.insertQuote(new Quote.Builder().withCounter(1).withBody("abcd").withOrigin("who").build());

        // when
        quoteDao.clearQuotes();

        // then
        List<Quote> quotes = quoteDao.findQuotes(null, null, null);
        Assert.assertEquals(quotes.size(), 0);
    }

    @Test
    public void findQuote() throws Exception {
        // given
        quoteDao.insertQuote(new Quote.Builder().withCounter(1).withBody("abcd").withOrigin("who").build());

        // when
        List<Quote> quotes = quoteDao.findQuotes(null, null, "who");

        // then
        Assert.assertEquals(1, quotes.size());
        Quote quote = quotes.get(0);
        Assert.assertNotNull(quote);
        QuoteAssert.assertThat(quote).hasBody("abcd").hasCounter(1).hasOrigin("who");
    }
}