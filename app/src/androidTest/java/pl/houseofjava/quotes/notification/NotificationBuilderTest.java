package pl.houseofjava.quotes.notification;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import pl.houseofjava.quotes.notification.dao.sql.QuoteDao;
import pl.houseofjava.quotes.notification.dao.sql.QuoteSQLiteHelper;
import pl.houseofjava.quotes.notification.model.Quote;
import pl.houseofjava.quotes.notification.ui.NotificationBuilder;
import pl.houseofjava.quotes.notification.util.QuoteAssert;

import static android.support.test.InstrumentationRegistry.getTargetContext;

@RunWith(AndroidJUnit4.class)
public class NotificationBuilderTest {

    private QuoteDao quoteDao;

    @Before
    public void setUp() throws Exception {
        getTargetContext().deleteDatabase(QuoteSQLiteHelper.DATABASE_NAME);
        quoteDao = new QuoteDao(getTargetContext(), "pol");
        quoteDao.clearQuotes();

        quoteDao.insertQuote(new Quote.Builder().withCounter(1).withBody("abcd").withOrigin("who").build());
        quoteDao.insertQuote(new Quote.Builder().withCounter(2).withBody("abcd").withOrigin("who2").build());
        quoteDao.insertQuote(new Quote.Builder().withCounter(2).withBody("abcd").withOrigin("who3").build());
    }

    @Test
    public void shouldBuildNotificationAndNotifyAndIncreaseCounter() throws Exception {
        // given
        NotificationBuilder builder = new NotificationBuilder(InstrumentationRegistry.getTargetContext());

        // when
        builder.buildNotificationAndNotify();

        // then
        List<Quote> quotes = quoteDao.findQuotes(null, null, "who");
        Assert.assertEquals(1, quotes.size());
        QuoteAssert.assertThat(quotes.get(0)).hasCounter(2);
    }
}